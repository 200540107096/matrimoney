import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}


class MyApp extends StatelessWidget {
  const MyApp({Key key}) : super(key: key);

  static const title = 'My Test App';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: title,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: title),
    );
  }
}


class MyHomePage extends StatefulWidget {
  const MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}


class _MyHomePageState extends State<MyHomePage> {

  @override
  void initState() {
    super.initState();
    // add anything you need here
  }

  @override
  Widget build(BuildContext context) {
    // this method is rerun every time setState is called
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),

      body: ListView(
        children: [

          // common icons
          // Icons.all_inclusive
          // Icons.insert_photo_outlined
          // Icons.settings
          // Icons.help_outline

          ListTile(
            leading: const Icon(Icons.all_inclusive),
            title: const Text('ListView Example'),
            // onTap: () => Navigator.push(
            //   context,
            //   MaterialPageRoute(builder: (context) => const LoadingNext()),
            // ),
          ),

          // ListTile(
          //   tileColor: const Color.fromARGB(8, 0, 0, 0),
          //   leading: const Icon(Icons.insert_photo_outlined),
          //   title: const Text('Manage Images'),
          //   onTap: () => Navigator.push(
          //     context,
          //     MaterialPageRoute(builder: (context) => const PhotoList()),
          //   ),
          // ),
        ],
      ),

      floatingActionButton: FloatingActionButton(
        tooltip: 'Add Image',
        onPressed: () {
        },
        child: const Icon(Icons.add),
      ),

    ); //build
  }
}