import 'package:flutter/material.dart';
import 'package:matrimoney/api/api.dart';
import 'package:matrimoney/database/add_user.dart';

class Bottom extends StatefulWidget {
  const Bottom({Key key}) : super(key: key);

  @override
  State<Bottom> createState() => _BottomState();
}

class _BottomState extends State<Bottom> {
  int _index = 0;
  final tab = [
    const Center(child: Text('Home')),
    const Center(child: Text('Crud')),
    const Api(),

  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: tab[_index],
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _index,
        iconSize: 32,
        backgroundColor: Colors.cyanAccent,
        selectedItemColor: Colors.black,
        items: [
          BottomNavigationBarItem(icon: Icon(Icons.home), label: "MyApp"),
          BottomNavigationBarItem(
            icon: IconButton(
              icon: Icon(Icons.add_circle),
              onPressed: (){Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=> AddUser()));},
            ),
            label: "Crud",),
          BottomNavigationBarItem(icon: Icon(Icons.api_sharp), label: "Api"),
        ],
        onTap: (index){
          setState(() {
            _index = index;
          });
        },
      ),
    );
  }
}
