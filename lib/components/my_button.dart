import 'package:flutter/material.dart';
import 'package:matrimoney/api/api.dart';
import 'package:matrimoney/components/bottom_navigation.dart';

class MyButton extends StatelessWidget {
  final Function() onTap;

  const MyButton({
    Super, key,
    this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(12),
      margin: const EdgeInsets.symmetric(horizontal: 15),
      decoration: BoxDecoration(
        color: Colors.black,
        borderRadius: BorderRadius.circular(10),
      ),
      child: TextButton(
        onPressed: () {
          Navigator.push(context, MaterialPageRoute(builder: (context) => Bottom()));
        },
        child: Text(
          "Sign In",
          style: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.bold,
            fontSize: 16,
          ),
        ),

        ),
      );

  }
}

class required {}
