// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:matrimoney/components/my_button.dart';
import 'package:matrimoney/database/detail.dart';
class ShowUser extends StatefulWidget {
  const ShowUser({Key key,  this.user}) : super(key: key);
  final User user;

  @override
  State<ShowUser> createState() => _ShowUserState();
}

class _ShowUserState extends State<ShowUser> {
  bool _isFavorited = false;

  void _toggleFavorite() {
    setState(() {
      _isFavorited = !_isFavorited;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          IconButton(
            icon: _isFavorited
                ? Icon(Icons.favorite)
                : Icon(Icons.favorite_border),
            color: Colors.redAccent,
            onPressed: _toggleFavorite,
          ),
        ],
        title: Text(widget.user.name),
        centerTitle: true,
      ),
      body: Container(
        color: Colors.black87,
        height: double.infinity,
        width: double.infinity,
        padding: EdgeInsets.all(14),
        child: Column(
          // mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text("ID :- ${widget.user.id}",
                style: TextStyle(fontSize: 20, color: Colors.white)),
            SizedBox(
              height: 15,
            ),
            Text("Name :- ${widget.user.name}",
                style: TextStyle(fontSize: 20, color: Colors.white)),
            SizedBox(
              height: 15,
            ),
            Text("Gender :- ${widget.user.gender}",
                style: TextStyle(fontSize: 20, color: Colors.white)),
            SizedBox(
              height: 15,
            ),
            Text("City :- ${widget.user.city}",
                style: TextStyle(fontSize: 20, color: Colors.white)),
            SizedBox(
              height: 15,
            ),
            Text("Description :- ${widget.user.description}",
                style: TextStyle(fontSize: 20, color: Colors.white)),
          ],
        ),
      ),
    );
  }
}
