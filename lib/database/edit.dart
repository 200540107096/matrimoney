// ignore_for_file: unused_field, library_private_types_in_public_api, prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:matrimoney/components/my_button.dart';
import 'package:matrimoney/database/db_helper.dart';
import 'package:matrimoney/database/detail.dart';
import 'package:matrimoney/database/user_list.dart';

class EditUser extends StatefulWidget {
  final User user;

  const EditUser({Key key,  this.user}) : super(key: key);

  @override
  _EditUserState createState() => _EditUserState();
}

class _EditUserState extends State<EditUser> {
  final _formKey = GlobalKey<FormState>();
  String _name;
  String _gender;
  String _city;
  String _description;

  @override
  void initState() {
    super.initState();
    _name = widget.user.name;
    _city = widget.user.city;
    _gender = widget.user.gender;
    _description = widget.user.description;
  }

  // String dropdownvalue = 'Select Gender';
  // var items = [
  //   'Select Gender',
  //   'Male',
  //   'Female',
  //   'Other',
  // ];
  String _genderValue = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Edit User'),
        leading: IconButton(
          onPressed: () {
            Navigator.pushReplacement(
                context, MaterialPageRoute(builder: (context) => UserList()));
          },
          icon: (Icon(Icons.arrow_back)),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              TextFormField(
                initialValue: _name,
                decoration: InputDecoration(labelText: 'Name'),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please enter name';
                  }
                  return null;
                },
                onSaved: (value) => _name = value,
              ),
              TextFormField(
                initialValue: _city,
                decoration: InputDecoration(labelText: 'City'),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please enter city';
                  }
                  return null;
                },
                onSaved: (value) => _city = value,
              ),
              SizedBox(height: 16.0),
              // TextFormField(
              //   initialValue: _gender,
              //   decoration: InputDecoration(labelText: 'gender'),
              //   validator: (value) {
              //     if (value == null || value.isEmpty) {
              //       return 'Please enter gender';
              //     }
              //     return null;
              //   },
              //   onSaved: (value) => _gender = value!,
              // ),
              // Text("Gender"),
              // SizedBox(height: 8),
              // DropdownButton(
              //   value: dropdownvalue,
              //   icon: const Icon(Icons.keyboard_arrow_down),
              //   items: items.map((String items) {
              //     return DropdownMenuItem(
              //       value: items,
              //       child: Text(items),
              //     );
              //   }).toList(),
              //   onChanged: (String? newValue) {
              //     setState(() {
              //       dropdownvalue = newValue!;
              //     });
              //   },
              // ),
              const Text(
                'Gender',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 16,
                ),
              ),
              Row(
                children: [
                  Expanded(
                    child: RadioListTile<String>(
                      title: Text('Male'),
                      value: 'Male',
                      groupValue: _genderValue,
                      onChanged: (value) {
                        setState(() {
                          _genderValue = value;
                        });
                      },
                    ),
                  ),
                  Expanded(
                    child: RadioListTile<String>(
                      title: Text('Female'),
                      value: 'Female',
                      groupValue: _genderValue,
                      onChanged: (value) {
                        setState(() {
                          _genderValue = value;
                        });
                      },
                    ),
                  ),
                ],
              ),
              SizedBox(height: 16.0),
              TextFormField(
                initialValue: _description,
                decoration: InputDecoration(labelText: 'description'),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please enter description';
                  }
                  return null;
                },
                onSaved: (value) => _description = value,
              ),
              SizedBox(height: 18),
              Container(
                padding: EdgeInsets.only(left: 105),
                child: FloatingActionButton.extended(
                  onPressed: _submitForm,
                  icon: const Icon(Icons.edit),
                  label: const Text('Edit Users'),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _submitForm() {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      _updateUser();
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => UserList()));
    }
  }

  void _updateUser() {
    final updatedUser = widget.user.copyWith(
        name: _name,
        city: _city,
        gender: _genderValue,
        description: _description);
    DatabaseProvider.db.update(updatedUser);
  }
}
