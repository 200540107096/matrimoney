// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:matrimoney/components/my_button.dart';
import 'package:matrimoney/database/detail.dart';

class LikedUsersPage extends StatelessWidget {
  final List<User> likedUsers;

  const LikedUsersPage({Key key,  this.likedUsers}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Liked Users'),
      ),
      body: ListView.builder(
        itemCount: likedUsers.length,
        itemBuilder: (context, index) {
          final user = likedUsers[index];
          return ListTile(
            title: Text(user.name),
            subtitle: Text(user.city),
          );
        },
      ),
    );
  }
}
